// @ts-check
const { test, expect } = require('./login-fixture.spec');

test.describe.configure({ mode: 'parallel' });
test.describe('Suite 1', () => {
  test('Test Login Successful', async ({ userPage }) => {
    await expect(userPage.locator('[data-test="sidenav-user-full-name"]')).toBeVisible();
    await userPage.pause();
    await expect(userPage.locator('[data-test="sidenav-user-full-name"]')).toContainText('Edgar J');
    await expect(userPage.locator('[data-test="sidenav-username"]')).toContainText('@Katharina_Bernier');
    
  });

  test('Test My Account Page', async ({ userPage }) => {
    await userPage.getByText('My Account').click();

    await expect(userPage.locator('[name="firstName"]')).toHaveValue('Edgar');
    await expect(userPage.locator('[name="lastName"]')).toHaveValue('Johns');
    await expect(userPage.locator('[name="email"]')).toHaveValue('Norene39@yahoo.com');
    await expect(userPage.locator('[name="phoneNumber"]')).toHaveValue('625-316-9882');

  });

  test('Test Transfer Money', async ({ userPage }) => {
    const currentBalance = await userPage.locator('[data-test="sidenav-user-balance"]').textContent();

    await userPage.locator('[data-test="nav-top-new-transaction"]').click();
    await userPage.getByText('Arely Kertzmann').click();
    await userPage.locator('[id="amount"]').fill('100');
    await userPage.locator('[id="transaction-create-description-input"]').fill('Test');
    await userPage.locator('[data-test="transaction-create-submit-payment"]').click();
    await userPage.reload();
    const newBanalce = await userPage.locator('[data-test="sidenav-user-balance"]').textContent();
    expect(parseFloat(newBanalce)).toBe(parseFloat(currentBalance) + 100);
  });

  test('Test Login Failure - Invalid Password', async ({ page }) => {
    await page.goto('http://localhost:3000/signin');
    await page.locator('[id="username"]').fill('Katharina_Bernier');
    await page.locator('[id="password"]').fill('test'); // invalid password
    await page.locator('[data-test="signin-submit"]').click();
    await expect(page.locator('[data-test="signin-error"]')).toBeVisible();
    await expect(page.locator('[data-test="signin-error"]')).toContainText('Username or password is invalid');
  });
});